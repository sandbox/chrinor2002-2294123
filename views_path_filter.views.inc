<?php

/**
 * @file
 * Views integration for the views_field_view module.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_path_filter_views_data_alter(&$data) {
  $field_info = field_info_field_types('text_long');
  $key_suffix = '_path_filter';
  foreach (field_info_fields() as $field) {
    if($field['type'] != 'text_long' || $field['storage']['type'] != 'field_sql_storage'){
      continue;
    }

    $src = &$data['field_data_'.$field['field_name']];
    $src_key_value = $field['field_name'].'_value';
    $src_key = $src_key_value.$key_suffix;
    $src[$src_key] = $src[$src_key_value];

    unset($src[$src_key]['argument']);

    $src[$src_key]['title'] .=' - page filter';
    $src[$src_key]['title short'] .=' - page filter';
    $src[$src_key]['filter']['handler'] ='views_path_filter_handler_filter_string';

    // since this is a known field we are able to do with. If the field changes, this will need to change.
    /*
    $root_data_key_src = 'field_data_'.$field['field_name'];
    $root_data_key = 'field_data_'.$field['field_name'].$key_suffix;
    $src = $data[$root_data_key_src];
    unset($src[$field['field_name']]);
    unset($src[$field['field_name'].'_format']);

    $value_key = $field['field_name'].'_value';

    unset($src[$value_key]['argument']);

    $src['entity_id']['field']['moved to'][0] = $root_data_key;
    unset($src['entity_id']['field']['moved to'][1]);

    $src[$value_key]['title'] .=' - page filter';
    $src[$value_key]['title short'] .=' - page filter';
    $src[$value_key]['filter']['handler'] ='views_path_filter_handler_filter_string';

    $result[$root_data_key] = $src;
    */

    //if (is_array($result)) {
      //$data = drupal_array_merge_deep($result, $data);
    //}
  }
  //var_dump($data);
  //die();
  /*$data['views']['view']['field'] = array(
    'title' => t('View'),
    'help' => t('Embed a view as a field. This can cause slow performance, so enable some caching.'),
    'handler' => 'views_field_view_handler_field_view',
  );
  $data['views']['view_field'] = array(
    'title' => t('View (Views field view)'),
    'help' => t('Embed a view in an area. This can cause slow performance, so enable some caching.'),
    'area' => array(
      'help' => t('Embed a views_field_view field in an area. This can also accept argument tokens.'),
      'handler' => 'views_field_view_handler_field_view',
    ),
  );*/
}

