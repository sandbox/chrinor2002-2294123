<?php

/**
 *
 * @ingroup views_filter_handlers
 */
class views_path_filter_handler_filter_string extends views_handler_filter_string {
  function can_expose(){
    return FALSE;
  }

  function option_definition() {
    $options = parent::option_definition();

    unset($options['expose']);

    return $options;
  }

  /**
   * See parent class description.
   */
  function operators() {
    $operators = array(
      'all' => array(
        'title' => t('All pages except those listed'),
        'short' => t('All except'),
        'method' => 'op_all',
        'values' => 1,
      ),
      'none' => array(
        'title' => t('Only the listed pages'),
        'short' => t('Only'),
        'method' => 'op_only',
        'values' => 1,
      ),
    );

    return $operators;
  }

  function admin_summary() {
    if ($this->is_a_group()) {
      return t('grouped');
    }

    $options = $this->operator_options('short');
    $output = '';
    if(!empty($options[$this->operator])) {
      $output = check_plain($options[$this->operator]);
    }
    return $output;
  }

  /**
   * Provide a simple textfield for equality
   */
  function value_form(&$form, &$form_state) {
    // do nothing
  }

  /**
   * 
   */
  function query() {
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";

    // Add the field.
    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();
    $this->field_alias = $this->query->add_field($this->table_alias, $this->real_field, NULL, $params);

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $fn_name = $info[$this->operator]['method'];
      if(method_exists($this, $fn_name)){
        $this->{$fn_name}($field);
      }
    }
  }

  function pre_execute(&$values){
    $field = "$this->table_alias.$this->real_field";

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $fn_name = $info[$this->operator]['method'].'_pre';
      if(method_exists($this, $fn_name)){
        $this->{$fn_name}($field);
      }
    }
    //dsm($values);
  }
  function post_execute(&$values){
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $fn_name = $info[$this->operator]['method'].'_post';
      if(method_exists($this, $fn_name)){
        $this->{$fn_name}($values);
      }
    }
    //dsm($values);
  }

  /**
   * Add this filter to the query.
   *
   * Due to the nature of fapi, the value and the operator have an unintended
   * level of indirection. You will find them in $this->operator
   * and $this->value respectively.
   */
  /*function query() {
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($field);
    }
  }*/

  //function op_all($field) {}
  //function op_all_pre($field) {}
  function op_all_post(&$values) {
    foreach($values as $i => $row){
      $patterns = $row->{$this->field_alias};
      $patterns.="\nadmin/structure/views/*";
      $page_match = $this->_compare(0, $patterns);
      if($page_match === FALSE){
        unset($values[$i]);
      }
    }
  }

  //function op_only($field) {}
  //function op_only_pre($field) {}
  function op_only_post(&$values) {
    foreach($values as $i => $row){
      $patterns = $row->{$this->field_alias};
      //$patterns.="\nadmin/structure/views/*";
      $page_match = $this->_compare(1, $patterns);
      if($page_match === FALSE){
        unset($values[$i]);
      }
    }
  }

//  function _get_paths($patterns){
//    // guts taken from core drupal_match_path(path.inc:312) as of V7.27
//
//    // Convert path settings to a regular expression.
//    // Therefore replace newlines with a logical or, /* with asterisks and the <front> with the frontpage.
//    $to_replace = array(
//      '/(\r\n?|\n)/', // newlines
//      '/\\\\\*/',     // asterisks
//      '/(^|\|)\\\\<front\\\\>($|\|)/' // <front>
//    );
//    $replacements = array(
//      //'|',
//      '.*',
//      '\1' . preg_quote(variable_get('site_frontpage', 'node'), '/') . '\2'
//    );
//    $lines = preg_split('/(\r\n?|\n)/', $patterns);
//    return $lines;
//  }

  function _compare($type, $patterns){
    // the guts of this section were taken from core block.module:808 as of V7.27
    
    // Convert path to lowercase. This allows comparison of the same path
    // with different case. Ex: /Page, /page, /PAGE.
    $pages = drupal_strtolower($patterns);
    // Convert the Drupal path to lowercase
    $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
    // Compare the lowercase internal and lowercase path alias (if any).
    $page_match = drupal_match_path($path, $pages);
    if ($path != $_GET['q']) {
      $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
    }
    // When $type has a value of 0, the items matches on all pages except those
    // listed in $this->value.
    // When set to 1, it is displayed only on thosepages listed in $this->value.
    $page_match = !($type xor $page_match);

    return $page_match;
  }
}
